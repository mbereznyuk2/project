#include <iostream>
class Vector
{
int *box;
int index = 0;
int current_size = 0;

public:
    Vector(int size){
        box = new int[size];
        current_size = size;
        }
    ~Vector(){
        delete [] box;
    }
    void fill(int begin, int end , int value){
        for (int b = begin, e = end; b < end; b++ , e-- ){
            box[b] = value ; box[e] = value;
            if (b >= e) break;
        }
    }
    void resize(int new_size){
        int new_box[new_size] ;
        for (int i = 0 ; i < current_size; i++){
            new_box[i] = box[i];
        }
        box = new int[new_size];
        for (int i = 0; i < current_size; i++){
            box[i] = new_box[i];
        }
        current_size = new_size;
    }
    void isOverflowPushBack(){
        if (index == current_size) resize(index+1);
    }
    void push_back(int x)
    {
        isOverflowPushBack();
        box[index++] = x;
    };

    int& operator[](int parameter)
    {
        return box[parameter];
    }

};

void printVector(Vector V){
    for (int i = 0 ; i < ((sizeof(V)/4)); i++){
        std::cout<< V[i]<<std::endl;
    }
}


int main()
{
    Vector V(2);
    V.push_back(11);
    V.push_back(12);
    V.push_back(13);
    V.push_back(19);
    V.fill(0, 1, 111);
    V.resize(20);
    V.push_back(19);
    for (int i = 0 ; i < 20;i++){
        std::cout<< V[i]<<std::endl;
    }


    // V.resize(20);
    // printVector(V);
    // std::cout << V[3]<<std::endl;
    return 0;
}